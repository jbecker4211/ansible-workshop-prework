#### FIREWALL RULE FOR BASTION ACCESS
resource "google_compute_firewall" "bastion-access" {
  name          = "bastion-ssh-access"
  network       = "${google_compute_network.prod-network.self_link}"
  source_ranges = ["10.1.0.0/16"]

  allow {
    protocol = "TCP"
    ports    = ["80"]
  }

  allow {
    protocol = "TCP"
    ports    = ["3128"]
  }

  allow {
    protocol = "TCP"
    ports    = ["3306"]
  }

  allow {
    protocol = "TCP"
    ports    = ["443"]
  }

  allow {
    protocol = "TCP"
    ports    = ["22"]
  }

  allow {
    protocol = "icmp"
  }

  description = "Bastion managed instance"
  disabled    = "false"
  project     = "${var.cloudreach_project}"
  priority    = "1000"
  target_tags = ["bastionmanaged"]
  direction   = "INGRESS"
}

#### FIREWALL RULE FOR EXTERNAL ACCESS BASTION

resource "google_compute_firewall" "external-access" {
  name          = "allow-external"
  network       = "${google_compute_network.prod-network.self_link}"
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "TCP"
    ports    = ["80"]
  }

  allow {
    protocol = "TCP"
    ports    = ["22"]
  }

  allow {
    protocol = "ICMP"
  }

  description = "Bastion Instance"
  disabled    = "false"
  project     = "${var.cloudreach_project}"
  priority    = "1000"
  target_tags = ["external"]
  direction   = "INGRESS"
}

#### FIREWALL RULE FOR EXTERNAL ACCESS PROD

resource "google_compute_firewall" "external-access-prod" {
  name          = "allow-external-prod"
  network       = "${google_compute_network.prod-network.self_link}"
  source_ranges = ["0.0.0.0/0"]

  allow {
    protocol = "TCP"
    ports    = ["80"]
  }

  allow {
    protocol = "TCP"
    ports    = ["3128"]
  }

  allow {
    protocol = "TCP"
    ports    = ["443"]
  }

  description = "nat allow Instance"
  disabled    = "false"
  project     = "${var.cloudreach_project}"
  priority    = "1000"
  target_tags = ["externalnat"]
  direction   = "INGRESS"
}
