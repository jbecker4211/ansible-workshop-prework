resource "google_compute_network" "prod-network" {
  name                    = "prod-network"
  auto_create_subnetworks = "false"
  routing_mode            = "GLOBAL"
  description             = "Production Network"
  project                 = "${var.cloudreach_project}"
}

resource "google_compute_subnetwork" "prod_subnetwork" {
  name                     = "ansible-network"
  ip_cidr_range            = "10.1.0.0/24"
  network                  = "${google_compute_network.prod-network.self_link}"
  region                   = "us-central1"
  description              = "ansible-subnet"
  project                  = "${var.cloudreach_project}"
  private_ip_google_access = "true"
  enable_flow_logs         = "false"
}
