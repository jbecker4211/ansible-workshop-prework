#### INSTANCE TEMPLATE FOR BASTION HOST

resource "google_compute_instance_template" "instance-template" {
  name        = "instances-template"
  description = "template for instance host"
  tags        = ["bastionmanaged", "nat"]

  disk {
    auto_delete  = "true"
    source_image = "debian-cloud/debian-9"
    mode         = "READ_WRITE"
    disk_type    = "pd-ssd"
    disk_size_gb = "25"
  }

  machine_type         = "n1-standard-1"
  can_ip_forward       = "false"
  instance_description = "bastion instance"

  network_interface {
    subnetwork = "${google_compute_subnetwork.prod_subnetwork.self_link}"
  }

  project = "${var.cloudreach_project}"
  region  = "us-central1"

  scheduling {
    automatic_restart   = "true"
    on_host_maintenance = "MIGRATE"
    preemptible         = "false"
  }
}

#### INSTANCE GROUP MANAGER

resource "google_compute_region_instance_group_manager" "instance-manager" {
  name               = "target-manager"
  base_instance_name = "instance"
  instance_template  = "${google_compute_instance_template.instance-template.self_link}"
  region             = "us-central1"
  project            = "${var.cloudreach_project}"
  update_strategy    = "ROLLING_UPDATE"

  rolling_update_policy {
    type           = "PROACTIVE"
    minimal_action = "RESTART"
  }

  target_size = "3"
}
