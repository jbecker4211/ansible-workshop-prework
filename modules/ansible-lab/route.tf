resource "google_compute_route" "nat-route" {
  name        = "nat-route"
  dest_range  = "0.0.0.0/0"
  network     = "${google_compute_network.prod-network.self_link}"
  tags        = ["nat"]
  priority    = "900"
  next_hop_ip = "${google_compute_instance.nat-gateway.network_interface.0.address}"
  project     = "${var.cloudreach_project}"
}
