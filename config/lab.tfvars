################# prod vpc ####################################

cloudreach_prod_vpc_name = "prodvpc"


################# bastion vpc ##################################

cloudreach_bastion_vpc_name = "bastionvpc"

################# Bastion subnetwork configurations #############

cloudreach_subnet_region_bastion = ["us-central1","us-west1","us-east1"]
cloudreach_subnet_ip_cidr_range_bastion = "10.250.0.0/16"

################## Production subnet configurations ##############

cloudreach_subnet_region_production = ["us-central1","us-east1","us-west1"]
cloudreach_subnet_ip_cidr_range_production = "10.1.0.0/16"


################# GOLBAL VARS ####################################
cloudreach_project = "cr-celab"
cloudreach_region = "us-central1"
cloudreach_zone = "us-central1-a"

################ Bastion Vars ####################################
cloudreach_instance_name = "jbbastion"


############### list of ranges to allow traffic from to bastion #############
cloudreach_firewall_source_ranges = ["0.0.0.0/0"]


cloudreach_bastion_metadata = {
    ssh-keys = "jamesbecker:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDr3k0QcwCCcekxVmISFTE/FLI/xQ9FFD+QgOVlBO8gvjWlhYVSb0x16oPIfWU2U08vLh+g5EgnCehuQ1cX3w3Gju34w/31qsCPd7m++taRTZKG05LqpwPgGK1lQIx8QZ7tDt8w8QRtoSwnLqvPZxqFCD5kM+ObB1CFB8od3uxZrUYUfmtntGVuOv6mI9HO41aRXcynjxZDpgIiNoAim2YJW++Fhj1lHEXXwpLSX9AncdUlqLN4dgJ6TpPTVw8cHh+eu2nk3TJedLl0p2mdlSQx1yxz9XFz5/nQj0roFt3JiAcF/Sv+TUBUAy+dkJfNYzcH1+W9WgLwZ1gu+VjpMmxB jamesbecker@James-Beckers-MacBook-Pro.local"
}
#### GKE VARIABLES
## Subnetwork 1
cloudreach_k8_subnet_name = "kubernetessubnet"
cloudreach_k8_subnet_ip_cidr_range  = "10.2.0.0/16"
cloudreach_k8_subnet_secondary_list = [{"ip_cidr_range"="10.4.0.0/16","range_name"="secondary1"},{"ip_cidr_range"="10.3.0.0/16","range_name"="secondary2"}]

## GKE variables 

cloudreach_k8_name = "jbeckerk8cluster"
cloudreach_k8_zone = "us-central1-a"
cloudreach_k8_initial_node_count = "2"
cloudreach_k8_additional_zones = ["us-central1-b","us-central1-c"]
cloudreach_k8_username = "jbecker"
cloudreach_k8_password = "P@ssw0rd1234567890"
cloudreach_k8_cluster_ipv4_cidr = "10.1.1.0/24"
cloudreach_k8_description = "jbeckerk8cluster"
cloudreach_k8_maintenance_window = "03:00"
cloudreach_k8_master_authorized_blocks = "35.225.15.40/32"
cloudreach_k8_master_cidr_block = "10.5.0.0/28"
cloudreach_k8_private_cluster = "true"

