provider "google" {
credentials = "${file("/Users/jamesbecker/ansible.json")}"
project = "${var.cloudreach_project}"
region = "us-central1"
}


module "ansible-lab" {

    source = "../modules/ansible-lab"
    cloudreach_project = "${var.cloudreach_project}"
}

